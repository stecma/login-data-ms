# LOGIN-DATA-MS
## Descripción
Este microservicio gestiona la carga y sincronización de datos entre la base de datos de ORACLE a la base de datos en memoria REDIS, las operaciones que se llevan a cabo en este servicio son:

*	Carga inicial de los datos de usuarios, servicios, grupos de servicios, menus y tablas maestras existentes en la base de datos ORACLE a la REDIS al iniciar la aplicación
*   Cambios en datos (INSERT,UPDATE,DELETE) que se realizan de la siguiente manera:
    
    1. El servicio que gestiona los cambios actualiza los datos se actualizan en REDIS
    2. El servicio que gestiona los cambios genera un evento en kafka que será consumido por este componente
    3. Con los datos del evento consumido se actualiza la base de datos de ORACLE
    

## Prerrequisitos
Para la correcta compilación y ejecución de la aplicación es necesario lo siguiente:

* Tener instalado maven 3 en nuestro equipo
* Tener configurado en el settings.xml de maven el repositorio nexus de globalia para poder acceder a los artefactos necesarios para la compilación del proyecto
* Tener instalado y configurado java 11 en nuestro equipo

## Construcción y ejecución del servicio
La construcción del servicio está basada en `maven`. Para la correcta compilación es necesario ejecutar el siguiente comando:

```bash prompt> clean install```

Para la compilación en local es necesario ejecutar el profile local para que no se compilen unas librerías que solo afectan a openshift y producen errores al levantar la aplicación en local:

```bash prompt> clean install -Plocal```


Tras ejecutar en el comando anterior, `maven` creará en la carpeta target/classes el jar del api "login-data-{VERSION}-exec.jar" así como las dependencias necesarias para la correcta ejecución del api (para ello se usan maven-jar-plugin y maven-dependency-plugin).

## Puesta en marcha del API
Para levantar el API se debe ejecutar el siguiente comando:

```bash prompt> java -jar target/classes/login-data-{VERSION}-exec.jar --spring.profiles.active=local --consul.ip={{consul.ip}} --kafka.address={{kafka.address}}```

Variables necesarias:

| Clave  | Descripción  | Valores posibles |
|---|---|---|
| spring.profiles.active | Perfil activo. | local | 
| consul.ip | Host de consul desde el que se recuperarán todos los endpoints a configurar en el proxy. | Dirección ip | 
| kafka.address | Listado de brokers de kafka. | IP:PUERTO | 

## Configuración
El LOGIN-DATA-MS usa las siguientes propiedades de configuración:

### bootstrap.yml
Para configurar la ruta de consul desde la que leer todas las propiedades de la aplicación se usa el archivo bootstrap.yml que contiene las siguientes propiedades:

| Clave  | Descripción  | Valores posibles |
|---|---|---|
| spring.application.name | Nombre de la aplicación | proxy |
| spring.cloud.consul.host | Host de consul | Dirección ip |
| spring.cloud.consul.port | Puesto de consul | Valor numérico |
| spring.cloud.consul.token | Token de acceso a consul | UUID |
| spring.cloud.consul.config.fail-fast | Si no se pueden cargar las propiedades de consul produce una excepción o un warning según el valor configurado | Valor booleano |
| spring.cloud.consul.config.enabled | Habilitar/Deshabilitar la configuración del consul | Valor booleano |
| spring.cloud.consul.config.prefix | Prefijo bajo el que se alojan las propiedades a recuperar | Cadena de texto |
| spring.cloud.consul.config.defaultContext | Contexto por defecto | Cadena de texto |

### application.yml
| Clave  | Descripción  | Valores posibles |
|---|---|---|
| server.error.whitelabel.enabled |  | Valor booleano |
| server.port | Puerto de acceso a los endpoints de la aplicación  | Valor numérico |
| spring.kafka.bootstrap-servers | Brokers de kafka | Lista de ips |
| spring.kafka.consumer.bootstrap-servers | Brokers de kafka | Lista de ips |
| spring.kafka.consumer.group-id | Id del grupo de kafka | String |
| spring.kafka.consumer.group-reply-id | Id del grupo de replicación de kafka | String |
| spring.kafka.consumer.auto-offset-reset | Configuración del último offset procesado | none,latest,earliest |
| spring.kafka.consumer.key-serializer | Serializador de key  | org.apache.kafka.common.serialization.StringSerializer |
| spring.kafka.consumer.value-serializer | Serializador del valor | org.apache.kafka.common.serialization.StringSerializer |
| spring.kafka.consumer.enable-auto-commit | Autoguardar el nuevo offset tras consumir el topic | Valor booleano |
| spring.kafka.consumer.auto-commit-interval | Frecuencia de guardado del offset | Milisegundos |
| spring.kafka.consumer.max-poll-interval-ms | Maximo tiempo entre actualización de un offset | Milisegundos |
| spring.kafka.consumer.max-poll-records | Maximo numero de eventos procesados antes de actualizar el offset | Valor numérico |
| spring.main.banner-mode | Mostrar el banner de spring | Valor booleano |
| spring.main.allow-bean-definition-overriding | Permitir sobreescritura de beans | Valor booleano |
| logging.level.root | Nivel de log para el root | INFO,DEBUG,WARNING,ERROR |
| logging.level.org.springframework.web | Nivel de log para el paquete web | INFO,DEBUG,WARNING,ERROR |
| logging.level.org.springframework.jdbc.core.JdbcTemplate | Nivel de log para el paquete jdbc.core.JdbcTemplate | INFO,DEBUG,WARNING,ERROR |
| logging.level.com.memorynotfound | Nivel de log para el paquete com.memorynotfound | INFO,DEBUG,WARN,ERROR |

## Logs
La gestión de logs se hace mediante Filebeat, para ello es necesario añadir el archivo log4j2-spring.xml con el siguiente contenido:


```<?xml version="1.0" encoding="UTF-8"?>
<Configuration>
	<Appenders>
		<Console name="Console" target="SYSTEM_OUT">
			<PatternLayout pattern="{&quot;timestamp&quot;:&quot;%d{ISO8601}&quot;,%m}%n"/>
		</Console>
	</Appenders>

	<Loggers>
		<Root level="info">
			<AppenderRef ref="Console"/>
		</Root>

		<Logger name="com.globalia" level="info"/>
	</Loggers>
</Configuration>```