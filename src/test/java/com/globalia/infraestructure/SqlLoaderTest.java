package com.globalia.infraestructure;

import com.globalia.HelperTest;
import com.globalia.infraestructure.SqlLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SqlLoaderTest extends HelperTest {

	@InjectMocks
	private SqlLoader sqlLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testLoaderNoQuery() {
		init(SqlLoader.class, this.sqlLoader, "menuSql", null);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoader() {
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoaderReload() {
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES_EMPTY);
		this.sqlLoader.loader();
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoaderReloadEqualsQuery() {
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testGetSqlEmptyFile() {
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES_EMPTY);
		this.sqlLoader.loader();
		assertNull(this.sqlLoader.getSql(SqlLoader.sqlFile.GROUP, SqlLoader.sqlType.QUERY_ALL));
	}

	@Test
	public void testGetSqlNoQuery() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES_EMPTY);
		this.sqlLoader.loader();
		TimeUnit.SECONDS.sleep(2);
		assertNull(this.sqlLoader.getSql(SqlLoader.sqlFile.MENU, SqlLoader.sqlType.QUERY_ALL));
	}

	@Test
	public void testGetSql() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "menuSql", HelperTest.QUERIES);
		this.sqlLoader.loader();
		TimeUnit.SECONDS.sleep(2);
		assertNotNull(this.sqlLoader.getSql(SqlLoader.sqlFile.MENU, SqlLoader.sqlType.QUERY_ALL));
	}
}
