package com.globalia.infraestructure;

import com.globalia.HelperTest;
import com.globalia.dto.ItemDao;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.infraestructure.SqlLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class RedisLoaderTest extends HelperTest {

	@InjectMocks
	private RedisLoader redisLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		init(RedisLoader.class, this.redisLoader, "groupsKey", "groupsKey");
		init(RedisLoader.class, this.redisLoader, "mastersKey", "mastersKey");
		init(RedisLoader.class, this.redisLoader, "menusKey", "menusKey");
		init(RedisLoader.class, this.redisLoader, "servicesKey", "servicesKey");
		init(RedisLoader.class, this.redisLoader, "usersKey", "usersKey");
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testLoaderNoItems() {
		when(this.getItems.getItems(any())).thenReturn(new ArrayList<>());
		this.redisLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoaderRuntimeException() {
		ItemDao item = new ItemDao();
		item.setId("1");
		item.setValue("json");

		ItemDao master = new ItemDao();
		master.setId("1¬COMPANY");
		master.setValue("json");

		when(this.getItems.getItems(any())).thenReturn(Collections.singletonList(item));
		when(this.getItems.getItems(SqlLoader.sqlFile.MASTER)).thenReturn(Collections.singletonList(master));
		doThrow(RuntimeException.class).when(this.client).addMap(anyString(), anyMap());
		this.redisLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testLoader() {
		ItemDao item = new ItemDao();
		item.setId("1");
		item.setValue("json");

		ItemDao master = new ItemDao();
		master.setId("1¬COMPANY");
		master.setValue("json");

		when(this.getItems.getItems(any())).thenReturn(Collections.singletonList(item));
		this.redisLoader.loader();
		assertTrue(true);
	}

	@Test
	public void testReload() {
		ItemDao item = new ItemDao();
		item.setId("1");
		item.setValue("json");

		ItemDao master = new ItemDao();
		master.setId("1¬COMPANY");
		master.setValue("json");

		when(this.getItems.getItems(any())).thenReturn(Collections.singletonList(item));
		this.redisLoader.reload();
		assertTrue(true);
	}
}
