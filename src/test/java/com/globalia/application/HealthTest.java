package com.globalia.application;

import com.globalia.HelperTest;
import com.globalia.application.Health;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.actuate.health.Status;
import org.springframework.jdbc.core.SingleColumnRowMapper;

import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings({"unchecked", "rawtypes"})
@RunWith(MockitoJUnitRunner.Silent.class)
public class HealthTest extends HelperTest {

	@InjectMocks
	private Health healthCheck;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testHealthDownRedis() {
		when(this.client.patternKeys(anyString())).thenThrow(RuntimeException.class);
		org.springframework.boot.actuate.health.Health result = this.healthCheck.health();
		assertEquals(Status.DOWN, result.getStatus());
	}

	@Test
	public void testHealthDownDb() {
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("1"));
		when(this.jdbcTemplate.query(anyString(), (SingleColumnRowMapper) any())).thenThrow(RuntimeException.class);
		org.springframework.boot.actuate.health.Health result = this.healthCheck.health();
		assertEquals(Status.DOWN, result.getStatus());
	}

	@Test
	public void testHealthUp() {
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("1"));
		when(this.jdbcTemplate.query(anyString(), (SingleColumnRowMapper) any())).thenReturn(List.of("1"));
		org.springframework.boot.actuate.health.Health result = this.healthCheck.health();
		assertEquals(Status.UP, result.getStatus());
	}
}
