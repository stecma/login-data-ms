package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.application.repository.SaveService;
import com.globalia.dto.Parameter;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.ServiceItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveServiceTest extends HelperTest {

	@InjectMocks
	private SaveService service;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testSendToBBDDCreateIOException() throws IOException {
		assertNotNull(this.sendToBBDD("create", true, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, true, false, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, true, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, false, true).getError());
	}

	@Test
	public void testSendToBBDDCreateItemNoParams() throws IOException {
		assertNull(this.sendToBBDD("create", false, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemEmptyParams() throws IOException {
		ServiceItem service = service();
		service.getUrl().setParams(new LinkedHashSet<>());
		assertNull(this.sendToBBDD(service, "create", false, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItem() throws IOException {
		ServiceItem service = service();
		service.getUrl().setParams(new LinkedHashSet<>(Set.of(new Parameter())));
		assertNull(this.sendToBBDD(service, "create", false, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemParams() throws IOException {
		Parameter param1 = new Parameter();
		param1.setKey("1");
		param1.setValue("1");
		Parameter param2 = new Parameter();
		param2.setKey("2");
		param2.setValue("2");

		ServiceItem service = service();
		service.getUrl().setParams(new LinkedHashSet<>(Set.of(param1, param2)));
		assertNull(this.sendToBBDD(service, "create", false, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateIOException() throws IOException {
		assertNotNull(this.sendToBBDD("update", true, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, true, false, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, false, true, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, false, false, true).getError());
	}

	@Test
	public void testSendToBBDDUpdateItem() throws IOException {
		assertNull(this.sendToBBDD("update", false, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteIOException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", true, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, true, false, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, false, true, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, false, false, true).getError());
	}

	@Test
	public void testSendToBBDDDeleteItem() throws IOException {
		assertNull(this.sendToBBDD("delete", false, false, false, false).getError());
	}

	private ItemResponse sendToBBDD(final String action, final boolean isIOException, final boolean isJsonException, final boolean isNoQuery, final boolean isRunException) throws IOException {
		return this.sendToBBDD(service(), action, isIOException, isJsonException, isNoQuery, isRunException);
	}

	private ItemResponse sendToBBDD(final ServiceItem service, final String action, final boolean isIOException, final boolean isJsonException, final boolean isNoQuery, final boolean isRunException) throws IOException {
		if (isIOException) {
			when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		} else {
			when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(service);
		}
		if (isJsonException) {
			when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		} else {
			when(this.jsonHandler.toJson(any())).thenReturn("json");
		}
		if (isNoQuery) {
			when(this.sqlLoader.getSql(any(), any())).thenReturn(null);
		} else {
			when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		}
		if (isRunException) {
			when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenThrow(RuntimeException.class);
		} else {
			when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenReturn(1);
		}
		return this.service.sendToBBDD(action, "json");
	}
}
