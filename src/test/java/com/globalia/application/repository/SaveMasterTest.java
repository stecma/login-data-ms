package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.application.repository.SaveMaster;
import com.globalia.dto.login.ItemResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveMasterTest extends HelperTest {

	@InjectMocks
	private SaveMaster master;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testSendToBBDDCreateIOException() throws IOException {
		assertNotNull(this.sendToBBDD("create", true, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, true, false, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, true, false).getError());
	}

	@Test
	public void testSendToBBDDCreateItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("create", false, false, false, true).getError());
	}

	@Test
	public void testSendToBBDDCreateItem() throws IOException {
		assertNull(this.sendToBBDD("create", false, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateIOException() throws IOException {
		assertNotNull(this.sendToBBDD("update", true, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, true, false, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, false, true, false).getError());
	}

	@Test
	public void testSendToBBDDUpdateItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("update", false, false, false, true).getError());
	}

	@Test
	public void testSendToBBDDUpdateItem() throws IOException {
		assertNull(this.sendToBBDD("update", false, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteIOException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", true, false, false, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemJsonProcessingException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, true, false, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemNoQuery() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, false, true, false).getError());
	}

	@Test
	public void testSendToBBDDDeleteItemRuntimeException() throws IOException {
		assertNotNull(this.sendToBBDD("delete", false, false, false, true).getError());
	}

	@Test
	public void testSendToBBDDDeleteItem() throws IOException {
		assertNull(this.sendToBBDD("delete", false, false, false, false).getError());
	}

	private ItemResponse sendToBBDD(final String action, final boolean isIOException, final boolean isJsonException, final boolean isNoQuery, final boolean isRunException) throws IOException {
		if (isIOException) {
			when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		} else {
			when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(master());
		}
		if (isJsonException) {
			when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		} else {
			when(this.jsonHandler.toJson(any())).thenReturn("json");
		}
		if (isNoQuery) {
			when(this.sqlLoader.getSql(any(), any())).thenReturn(null);
		} else {
			when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		}
		if (isRunException) {
			when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenThrow(RuntimeException.class);
		} else {
			when(this.jdbcTemplate.update(anyString(), (Object[]) any())).thenReturn(1);
		}
		return this.master.sendToBBDD(action, "json");
	}
}
