package com.globalia.application.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.application.Health;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MessageConsumerReplyTest extends HelperTest {

	@InjectMocks
	private MessageConsumerReply consumer;
	@Mock
	private Health health;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testProcessMessageIOException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		assertNull(this.consumer.processMessage("health"));
	}

	@Test
	public void testProcessMessageJsonProcessingException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("health"));
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNull(this.consumer.processMessage("health"));
	}

	@Test
	public void testProcessMessage() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("health"));
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNotNull(this.consumer.processMessage("health"));
	}
}
