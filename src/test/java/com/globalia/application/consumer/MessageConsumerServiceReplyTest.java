package com.globalia.application.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.login.ItemResponse;
import com.globalia.application.repository.SaveService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MessageConsumerServiceReplyTest extends HelperTest {

	@InjectMocks
	private MessageConsumerReply consumer;
	@Mock
	private SaveService service;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testProcessMessageIOException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		assertNull(this.consumer.processMessage("service"));
	}

	@Test
	public void testProcessMessageJsonProcessingException() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("service"));
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		when(this.service.sendToBBDD(anyString(), anyString())).thenReturn(new ItemResponse());
		assertNull(this.consumer.processMessage("service"));
	}

	@Test
	public void testProcessMessage() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message("service"));
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.service.sendToBBDD(anyString(), anyString())).thenReturn(new ItemResponse());
		assertNotNull(this.consumer.processMessage("service"));
	}
}
