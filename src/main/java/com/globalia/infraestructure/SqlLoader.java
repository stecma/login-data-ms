package com.globalia.infraestructure;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RefreshScope
@Service
public class SqlLoader {

	private final ExecutorService pool = Executors.newFixedThreadPool(sqlFile.values().length);
	private final Map<String, Map<String, String>> queriesMap = new HashMap<>();
	private final Map<String, String> queryMap = new HashMap<>();

	@Value("${groupSql}")
	private String groupSql;
	@Value("${masterSql}")
	private String masterSql;
	@Value("${menuSql}")
	private String menuSql;
	@Value("${serviceSql}")
	private String serviceSql;
	@Value("${userSql}")
	private String userSql;

	public void loader() {
		CompletableFuture.allOf(Arrays.stream(sqlFile.values()).map(v -> this.loadSQLQueries(this.getValue(v), v)).toArray(CompletableFuture[]::new));
	}

	public String getSql(final sqlFile file, final sqlType sqlType) {
		return this.queriesMap.containsKey(file.name()) && this.queriesMap.get(file.name()).containsKey(sqlType.name().toLowerCase()) ? this.queriesMap.get(file.name()).get(sqlType.name().toLowerCase()) : null;
	}

	private String getValue(final sqlFile file) {
		switch (file) {
			case SERVICE:
				return this.serviceSql;
			case GROUP:
				return this.groupSql;
			case MENU:
				return this.menuSql;
			case USER:
				return this.userSql;
			default:
				return this.masterSql;
		}
	}

	@Async
	CompletableFuture<Void> loadSQLQueries(final String queries, final sqlFile file) {
		return CompletableFuture.supplyAsync(() -> {
			if (StringUtils.hasText(queries)) {
				SqlLoader.log.info(String.format("Init load queries from %s", file.name()));
				if (!queries.equalsIgnoreCase(this.queryMap.get(file.name()))) {
					this.getSqlSentence(file.name(), queries);
				}
				SqlLoader.log.info(String.format("Load complete %s", file.name()));
			}
			return null;
		}, this.pool);
	}

	private void getSqlSentence(final String file, final String queries) {
		if (!this.queryMap.containsKey(file)) {
			this.queryMap.put(file, "");
			this.queriesMap.put(file, null);
		}
		this.queryMap.replace(file, queries);

		JsonObject json = (JsonObject) JsonParser.parseString(queries);
		Map<String, String> map = null;
		for (JsonElement query : json.get("queries").getAsJsonArray()) {
			if (map == null) {
				map = new HashMap<>();
			}
			map.computeIfAbsent(query.getAsJsonObject().get("type").getAsString(), k -> query.getAsJsonObject().get("sql").getAsString());
		}
		this.queriesMap.replace(file, map);
	}

	public enum sqlFile {
		GROUP,
		MASTER,
		MENU,
		SERVICE,
		USER
	}

	public enum sqlType {
		QUERY_ALL,
		INSERT,
		UPDATE,
		DELETE
	}
}