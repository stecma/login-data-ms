package com.globalia.infraestructure;

import com.globalia.Context;
import com.globalia.application.repository.GetItems;
import com.globalia.dto.ItemDao;
import com.globalia.enumeration.LogType;
import com.globalia.redis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RefreshScope
@Service
public class RedisLoader {

	private final ExecutorService pool = Executors.newFixedThreadPool(SqlLoader.sqlFile.values().length);
	private static final String REDIS_KEY_FORMAT = "%s:%s";

	@Value("${logsFormat}")
	private String logsFormat;
	@Value("${redis.groupsKey}")
	private String groupsKey;
	@Value("${redis.mastersKey}")
	private String mastersKey;
	@Value("${redis.menusKey}")
	private String menusKey;
	@Value("${redis.servicesKey}")
	private String servicesKey;
	@Value("${redis.usersKey}")
	private String usersKey;

	@Autowired
	private GetItems getItems;
	@Autowired
	private RedisClient client;
	@Autowired
	private Context context;

	public void loader() {
		CompletableFuture.allOf(Arrays.stream(SqlLoader.sqlFile.values()).map(v -> this.loadRedis(this.getKey(v), this.getItems.getItems(v))).toArray(CompletableFuture[]::new));
	}

	public void reload() {
		this.client.flushDb();
		this.loader();
	}

	private String getKey(final SqlLoader.sqlFile file) {
		switch (file) {
			case SERVICE:
				return this.servicesKey;
			case GROUP:
				return this.groupsKey;
			case MENU:
				return this.menusKey;
			case USER:
				return this.usersKey;
			default:
				return this.mastersKey;
		}
	}

	@Async
	CompletableFuture<Void> loadRedis(final String redisKey, final List<ItemDao> items) {
		return CompletableFuture.supplyAsync(() -> {
			this.addAllRedis(redisKey, items, !redisKey.contains(SqlLoader.sqlFile.MASTER.name().toLowerCase()));
			return null;
		}, this.pool);
	}

	protected void addAllRedis(final String baseKey, final List<ItemDao> items, final boolean simpleMap) {
		Map<String, Map<String, Object>> map = this.getMap(items, simpleMap);
		if (!map.isEmpty()) {
			for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
				String finalKey = baseKey;
				if (StringUtils.hasText(entry.getKey())) {
					finalKey = String.format(RedisLoader.REDIS_KEY_FORMAT, baseKey, entry.getKey());
				}
				this.addMap(finalKey, entry.getValue());
			}
		}
	}

	private Map<String, Map<String, Object>> getMap(final List<ItemDao> items, final boolean simpleMap) {
		Map<String, Map<String, Object>> map = new HashMap<>();
		if (!items.isEmpty()) {
			String keyMap = "";
			for (ItemDao item : items) {
				String key = item.getId();
				if (!simpleMap) {
					String[] keys = item.getId().split("¬");
					keyMap = keys[1];
					key = keys[0];
				}
				map.computeIfAbsent(keyMap, k -> new HashMap<>());
				map.get(keyMap).computeIfAbsent(key, k -> item.getValue());
			}
		}
		return map;
	}

	private void addMap(final String finalKey, final Map<String, Object> map) {
		try {
			String temporaryKey = this.getRediskey(finalKey, true);
			String rediskey = this.getRediskey(finalKey, false);
			RedisLoader.log.info(String.format("Load redis %s", temporaryKey));
			this.client.addMap(temporaryKey, map);
			this.client.rename(temporaryKey, rediskey);
			RedisLoader.log.info(String.format("Load completed %s", rediskey));
		} catch (RuntimeException r) {
			RedisLoader.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format("Error: %s", r.getLocalizedMessage()));
		}
	}

	private String getRediskey(final String redisKey, final boolean isNew) {
		return String.format(isNew ? "%s:%s_new" : RedisLoader.REDIS_KEY_FORMAT, this.context.getEnvironment(), redisKey);
	}
}