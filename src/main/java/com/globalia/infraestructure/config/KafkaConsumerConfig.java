package com.globalia.infraestructure.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
@RefreshScope
public class KafkaConsumerConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String servers;
	@Value("${spring.kafka.consumer.group-id}")
	private String groupId;
	@Value("${spring.kafka.consumer.group-reply-id}")
	private String groupReplyId;
	@Value("${spring.kafka.consumer.auto-offset-reset}")
	private String offSet;
	@Value("${spring.kafka.consumer.enable-auto-commit}")
	private String autoCommit;
	@Value("${spring.kafka.consumer.auto-commit-interval}")
	private String commitInterval;
	@Value("${spring.kafka.consumer.max-poll-records}")
	private String maxPoll;
	@Value("${kafka.topicreplyname}")
	private String replyTopic;

	@Bean
	public ConsumerFactory<String, String> consumerFactory() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, this.servers);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, this.groupId);
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, this.offSet);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, this.autoCommit);
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, this.commitInterval);
		props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "5000");
		props.put(ConsumerConfig.RETRY_BACKOFF_MS_CONFIG, "5000");
		props.put(ConsumerConfig.RECONNECT_BACKOFF_MS_CONFIG, "5000");
		props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "5000");
		props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "25000");
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, this.maxPoll);
		return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new StringDeserializer());
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(this.consumerFactory());
		return factory;
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory(final KafkaTemplate<String, String> kafkaTemplate, final ConsumerFactory<String, byte[]> consumerFactory) {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, this.servers);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, this.groupReplyId);
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, this.offSet);

		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new StringDeserializer()));
		factory.setReplyTemplate(kafkaTemplate);
		return factory;
	}

	@Bean
	public KafkaTemplate<String, String> kafkaTempate(final ProducerFactory<String, String> producerFactory) {
		return new KafkaTemplate<>(producerFactory) {

			@Override
			public ListenableFuture<SendResult<String, String>> send(final String topic, final String data) {
				return super.send(topic, "some_generated_key", data);
			}
		};
	}
}