package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveMaster extends AbstractSaveDao<MasterItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final MasterItem item) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.INSERT) {
			return new Object[]{item.getId(), item.getEntity(), getJsonHandler().toJson(item)};
		}
		return new Object[]{getJsonHandler().toJson(item), item.getId(), item.getEntity()};
	}

	@Override
	protected SqlLoader.sqlFile getSqlFile(final MasterItem item) {
		return SqlLoader.sqlFile.MASTER;
	}

	@Override
	protected MasterItem getObject(final String json) throws IOException {
		return (MasterItem) getJsonHandler().fromJson(json, MasterItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final MasterItem item, final SqlLoader.sqlType sqlType) {
		response.setMaster(item);
	}
}