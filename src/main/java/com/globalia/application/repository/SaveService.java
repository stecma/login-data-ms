package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.Parameter;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.Url;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveService extends AbstractSaveDao<ServiceItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final ServiceItem item) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.INSERT) {
			return new Object[]{item.getId(), item.getEnvironment(), this.getRoute(item.getUrl()), getJsonHandler().toJson(item)};
		}
		return new Object[]{getJsonHandler().toJson(item), this.getRoute(item.getUrl()), item.getId()};
	}

	@Override
	protected SqlLoader.sqlFile getSqlFile(final ServiceItem item) {
		return SqlLoader.sqlFile.SERVICE;
	}

	@Override
	protected ServiceItem getObject(final String json) throws IOException {
		return (ServiceItem) getJsonHandler().fromJson(json, ServiceItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final ServiceItem item, final SqlLoader.sqlType sqlType) {
		response.setService(item);
	}

	private String getRoute(final Url url) {
		if (url.getParams() == null) {
			return url.getRoute();
		}

		int count = 0;
		StringBuilder route = new StringBuilder(url.getRoute());
		for (Parameter param : url.getParams()) {
			if (count > 0) {
				route.append("&");
			}
			route.append(param.getValue()).append("=").append(param.getValue());
			count++;
		}
		return route.toString();
	}
}