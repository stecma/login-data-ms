package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveGroup extends AbstractSaveDao<GroupItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final GroupItem item) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.INSERT) {
			return new Object[]{item.getId(), item.getEnvironment(), getJsonHandler().toJson(item)};
		}
		return new Object[]{getJsonHandler().toJson(item), item.getId()};
	}

	@Override
	protected SqlLoader.sqlFile getSqlFile(final GroupItem item) {
		return SqlLoader.sqlFile.GROUP;
	}

	@Override
	protected GroupItem getObject(final String json) throws IOException {
		return (GroupItem) getJsonHandler().fromJson(json, GroupItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final GroupItem item, final SqlLoader.sqlType sqlType) {
		response.setGroup(item);
	}
}