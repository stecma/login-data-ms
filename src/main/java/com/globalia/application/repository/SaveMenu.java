package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MenuItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveMenu extends AbstractSaveDao<MenuItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final MenuItem item) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.INSERT) {
			return new Object[]{item.getId(), item.getEnvironment(), item.getCompany(), getJsonHandler().toJson(item)};
		}
		return new Object[]{getJsonHandler().toJson(item), item.getCompany(), item.getId()};
	}

	@Override
	protected SqlLoader.sqlFile getSqlFile(final MenuItem item) {
		return SqlLoader.sqlFile.MENU;
	}

	@Override
	protected MenuItem getObject(final String json) throws IOException {
		return (MenuItem) getJsonHandler().fromJson(json, MenuItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final MenuItem item, final SqlLoader.sqlType sqlType) {
		response.setMenu(item);
	}
}