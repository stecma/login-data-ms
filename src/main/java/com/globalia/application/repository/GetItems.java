package com.globalia.application.repository;

import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.ItemDao;
import com.globalia.application.mapper.ItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Dao
@Component
public class GetItems {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private SqlLoader sqlLoader;

	public List<ItemDao> getItems(final SqlLoader.sqlFile file) {
		return this.getAllItems(file);
	}

	protected List<ItemDao> getAllItems(final SqlLoader.sqlFile file) {
		List<ItemDao> items = null;
		String query = this.sqlLoader.getSql(file, SqlLoader.sqlType.QUERY_ALL);
		if (StringUtils.hasText(query)) {
			items = this.jdbcTemplate.query(query, new ItemMapper());
		}
		return items;
	}
}