package com.globalia.application.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.aspect.Dao;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.UserItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Dao
@Component
public class SaveUser extends AbstractSaveDao<UserItem> {

	public Object[] getParams(final SqlLoader.sqlType sqlType, final UserItem item) throws JsonProcessingException {
		if (sqlType == SqlLoader.sqlType.INSERT) {
			return new Object[]{item.getId(), getJsonHandler().toJson(item), String.format("%s %s", item.getName(), item.getSurname())};
		} else if (sqlType == SqlLoader.sqlType.UPDATE) {
			return new Object[]{getJsonHandler().toJson(item), String.format("%s %s", item.getName(), item.getSurname()), item.getId()};
		}
		return new Object[]{getJsonHandler().toJson(item), item.getId()};
	}

	@Override
	protected SqlLoader.sqlFile getSqlFile(final UserItem item) {
		return SqlLoader.sqlFile.USER;
	}

	@Override
	protected UserItem getObject(final String json) throws IOException {
		return (UserItem) getJsonHandler().fromJson(json, UserItem.class);
	}

	@Override
	protected void setResult(final ItemResponse response, final UserItem item, final SqlLoader.sqlType sqlType) {
		response.setUser(item);
	}
}