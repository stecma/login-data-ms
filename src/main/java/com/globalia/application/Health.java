package com.globalia.application;

import com.globalia.Context;
import com.globalia.redis.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Service;

@Service
public class Health implements HealthIndicator {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private RedisClient client;
	@Autowired
	private Context context;

	@Override
	public org.springframework.boot.actuate.health.Health health() {
		if (this.redisCheck() < 0) {
			return org.springframework.boot.actuate.health.Health.down().withDetail("Error redis connection", 500).build();
		}
		if (this.dbCheck() < 1) {
			return org.springframework.boot.actuate.health.Health.down().withDetail("Error BBDD connection", 500).build();
		}
		return org.springframework.boot.actuate.health.Health.up().build();
	}

	private int redisCheck() {
		try {
			return this.client.patternKeys(String.format("%s:*", this.context.getEnvironment())).size();
		} catch (RuntimeException data) {
			return -1;
		}
	}

	@SuppressWarnings("SqlNoDataSourceInspection")
	private int dbCheck() {
		try {
			return this.jdbcTemplate.query("SELECT 1 FROM dual", new SingleColumnRowMapper<>()).size();
		} catch (RuntimeException data) {
			return 0;
		}
	}
}