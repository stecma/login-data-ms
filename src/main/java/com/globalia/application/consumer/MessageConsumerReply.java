package com.globalia.application.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.application.AbstractSaveDao;
import com.globalia.Context;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.dto.KafkaItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.LogType;
import com.globalia.application.repository.SaveGroup;
import com.globalia.application.Health;
import com.globalia.json.JsonHandler;
import com.globalia.application.repository.SaveMaster;
import com.globalia.application.repository.SaveMenu;
import com.globalia.application.repository.SaveService;
import com.globalia.application.repository.SaveUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RefreshScope
@Slf4j
public class MessageConsumerReply {

	@Value("${logsFormat}")
	private String logsFormat;

	@Autowired
	private Context context;
	@Autowired
	private JsonHandler jsonHandler;
	@Autowired
	private Health health;
	@Autowired
	private SaveService service;
	@Autowired
	private SaveGroup group;
	@Autowired
	private SaveMenu menu;
	@Autowired
	private SaveUser user;
	@Autowired
	private SaveMaster master;
	@Autowired
	private RedisLoader redis;

	@KafkaListener(topics = "${kafka.topicname}")
	@SendTo("${kafka.topicreplyname}")
	public String processMessage(final String message) {
		MessageConsumerReply.log.info(String.format("Received content: %s", message));
		try {
			KafkaItem item = (KafkaItem) this.jsonHandler.fromJson(message, KafkaItem.class);
			switch (item.getEntity()) {
				case "service":
					return this.serviceProcess(item);
				case "group":
					return this.groupProcess(item);
				case "menu":
					return this.menuProcess(item);
				case "user":
					return this.userProcess(item);
				case "master":
					return this.masterProcess(item);
				case "reload":
					this.redis.reload();
					return "OK";
				default:
					return this.healthProcess(item);
			}
		} catch (IOException e) {
			MessageConsumerReply.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(AbstractSaveDao.EXCEPTION_SER_MSG, e.getMessage()));
		}
		return null;
	}

	private String healthProcess(final KafkaItem item) throws JsonProcessingException {
		item.setJson(this.jsonHandler.toJson(this.health.health()));
		return this.jsonHandler.toJson(item);
	}

	private String serviceProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.service.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getService());
	}

	private String groupProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.group.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getGroup());
	}

	private String menuProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.menu.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getMenu());
	}

	private String userProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.user.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getUser());
	}

	private String masterProcess(final KafkaItem item) throws JsonProcessingException {
		ItemResponse response = this.master.sendToBBDD(item.getAction(), item.getJson());
		return this.jsonHandler.toJson(response.getMaster());
	}
}